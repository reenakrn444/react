import React, { useState } from 'react';

const VEG = 'VEG';
const NONVEG = 'NONVEG';


export default function Products({ setCart, cart }) {
  const [products] = useState([
    {
      category: NONVEG,
      name: 'Fish Duglere',
      cost: 210,
      image:
        'https://c.ndtvimg.com/2018-09/7a5i51c8_fish_625x300_07_September_18.jpg',
    },
    {
      category: NONVEG,
      name: 'Hariyali Machli With Onion Pulao',
      cost: 150,
      image:
        'https://i.ndtvimg.com/i/2016-07/hariyali-machchi_625x350_51469695719.jpg',
    },
    {
      category: VEG,
      name: 'IDly vada',
      cost: 30,
      image:
        'https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/gy6wnt46ipcvgacsuh6y',
    },
    {
      category: VEG,
      name: 'Masala Dosa',
      cost: 30,
      image:
        'https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/l4nozuuucgqmdc9t1fh3',
    },
    {
      category: VEG,
      name: 'Poori',
      cost: 30,
      image:
        'https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/m3yuglgr3dbarjht5al5',
    },
  ]);

  const addToCart = (product) => {
    let newCart = [...cart];
    let itemInCart = newCart.find(
      (item) => product.name === item.name
    );
    if (itemInCart) {
      itemInCart.quantity++;
    } else {
      itemInCart = {
        ...product,
        quantity: 1,
      };
      newCart.push(itemInCart);
    }
    setCart(newCart);
  };

  const [category, setCategory] = useState(VEG);

  const getProductsInCategory = () => {
    return products.filter(
      (product) => product.category === category
    );
  };

  return (
    <>
      <h1>Products</h1>
      Select a category
      <select onChange={(e) => setCategory(e.target.value)}>
        <option value={VEG}>{VEG}</option>
        <option value={NONVEG}>{NONVEG}</option>
      </select>
      <div className="products">
        {getProductsInCategory().map((product, idx) => (
          <div className="product" key={idx}>
            <h3>{product.name}</h3>
            <h4>Rs.{product.cost}</h4>
            <img src={product.image} alt={product.name} className="imgw"/>
            <button onClick={() => addToCart(product)}>
              Add to Cart
            </button>
          </div>
        ))}
      </div>
    </>
  );
}
